import React, { Component } from "react";
import cat from "../../img/cat.png";
import "./styles.scss";

class MainItem extends Component {
  static defaultProps = {
    text: "Чего сидишь? Порадуй котэ,",
    gift: "",
    extra: false,
    portions: ""
  };
  state = {
    hover: false,
    selected: false,
    disabled: false,
    wasSelected: false
  };
  componentDidMount() {
    const selected = this.props.selected || this.state.selected;
    const disabled = this.props.disabled || this.state.disabled;
    this.setState({ selected, disabled, wasSelected: selected });
  }
  handleClick = e => {
    const { disabled, wasSelected } = this.state;
    if (disabled) return;
    this.setState({ selected: !this.state.selected });
    e.preventDefault();
    if (!wasSelected) {
      console.log(e.target.closest(".mainItemRoot"));
      e.target.closest(".mainItemRoot").addEventListener("mouseleave", () => this.setState({ wasSelected: true }), {
        once: true
      });
    } else this.setState({ wasSelected: true });
  };
  handleMouseEnter = () => {
    this.setState({ hover: true });
  };
  handleMouseLeave = () => {
    this.setState({ hover: false });
  };
  changeSelect = () => {};
  render() {
    const { type, mass, portions, gift, extra, selectedComment } = this.props;
    const { selected, wasSelected, disabled, hover } = this.state;
    return (
      <div className={"mainItemRoot" + (wasSelected ? " wasSelected" : "")}>
        <div
          className={"mainItemCard" + (disabled ? " disabled" : selected ? " selected" : "") + (hover ? " hover" : "")}
          onClick={this.handleClick}
          onMouseEnter={this.handleMouseEnter}
          onMouseLeave={this.handleMouseLeave}
        >
          <div className="mainItemCardBackground" />
          {disabled && (
            <div className="disabledOverlay">
              <div className="disabledOverlayBackground" />
            </div>
          )}
          <div className="mainItem">
            <div className="mainItemBackground">
              <div className="description">
                {!(selected && hover) ? (
                  <span className="text1 muted">Сказочное заморское яство</span>
                ) : (
                  <span className="text1 selectedText">Котэ не одобряет?</span>
                )}
                <h1 className="text2">Нямушка</h1>
                <h4 className="text3">{type}</h4>
                {!!portions && <span className="text1 addons muted">{portions}</span>}
                {!!gift && <span className="text1 addons muted">{gift} в подарок</span>}
                {extra && <span className="text1 addons muted">Заказчик доволен</span>}
              </div>
            </div>

            <div className="badge">
              <span className="mass">{mass}</span>
              <span className="kg">кг</span>
            </div>
            <img className="cat" src={cat} alt="cat" />
          </div>
        </div>
        <div className={"comment" + (disabled ? " disabled" : "")}>
          {!disabled ? (
            selected ? (
              selectedComment
            ) : (
              <>
                Чего сидишь? Порадуй котэ,{" "}
                <b>
                  <a href="#" className="buy" onClick={this.handleClick}>
                    Купи.
                  </a>
                </b>
              </>
            )
          ) : (
            `Печалька, ${type} закончился`
          )}
        </div>
      </div>
    );
  }
}

export { MainItem };
