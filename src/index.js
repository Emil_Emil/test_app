import React from "react";
import ReactDOM from "react-dom";
import "react-app-polyfill/ie11";
import elementClosest from "element-closest";
import "./css/TrebuchetMS.css";
import "./css/Exo2_0Thin.css";
import "./css/index.scss";
import App from "./App";

elementClosest(window);

ReactDOM.render(<App />, document.getElementById("root"));
