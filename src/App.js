import React, { Component } from "react";
import { MainItem } from "./components";
import "./css/App.css";

const items = [
  {
    type: "с фуа-гра",
    selectedComment: "Печень утки разварная с артишоками.",
    mass: "0,5",
    selected: false,
    portions: "10 порций",
    gift: "мышь"
  },
  {
    type: "с рыбой",
    selectedComment: "Головы щучьи с чесноком да свежайшая семгушка.",
    mass: "2",
    selected: false,
    portions: "40 порций",
    gift: "2 мыши"
  },
  {
    type: "с курой",
    selectedComment: "Филе из цыплят с трюфелями в бульоне.",
    mass: "5",
    selected: false,
    portions: "100 порций",
    gift: "5 мышей",
    extra: true,
    disabled: true
  }
];
class App extends Component {
  render() {
    return (
      <>
        <h1 className="title textShadow">Ты сегодня покормил кота?</h1>
        <div className="grid">
          {items.map(item => (
            <MainItem key={item.type} {...item} />
          ))}
        </div>
      </>
    );
  }
}

export default App;
